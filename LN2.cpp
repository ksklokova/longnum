//LN2.cpp 
//���������� ����������, ����������� ������� � �������� ������� � ������� ������ LongNum � ���������� ��������� +-*/%

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LNDll.h"

#pragma comment (lib, "LNDll.lib")

class LongNum
{
private:
	bool bin;
	BigInt dnum;
	char *bnum;

public:
	//�����������
	LongNum();
	//����������� �����������
	LongNum (LongNum&);
	//������������
	LongNum& operator=(LongNum&);
	//����������
	~LongNum();

	//������������� �������� ����
	void rename_b(char *newname);
	//���� �� �����
	void input(char *, bool);
	//����� � ���������� ����
	void output(char *);

	//��������
	LongNum operator+(LongNum&) const;
	//���������
	LongNum operator-(LongNum&) const;
	//���������
	LongNum operator*(LongNum&) const;
	//�������
	LongNum operator/(LongNum&) const;
	//������� �� �������
	LongNum operator%(LongNum&) const;
	//���������� � ������� �� ������
	LongNum deg_by_mod(LongNum, LongNum);
};
	//�����������
LongNum::LongNum()
	{
		//bin=false;
		dnum.amount=0;
		dnum.minus=false;
		bnum=NULL;
	}
	//����������� �����������
LongNum::LongNum(LongNum& a)
	{
		bin=a.bin;
		if (bin)
		{
			
			//FILE *num1, *num2;
			//if (((num1=fopen(bnum, "wb"))==NULL)||((num2=fopen(a.bnum, "rb"))==NULL))
			//	perror("Can not open File");
			//int len1, len2, i;
			//unsigned char d1;
			//fseek(num1, 0, SEEK_END);
			//len1=ftell(num1);
			//fseek(num2, 0, SEEK_END);
			//len2=ftell(num2);
			//if (len1>len2)
			//{
			//	for (i=0; i<len1; i++)
			//		if (i<len2)
			//		{
			//			fseek(num2, (len2-i)*sizeof_base, 0);
			//			fread(&d1, sizeof_base, 1, num2);
			//			fseek(num1, (len1-i)*sizeof_base, 0);
			//			fwrite(&d1, sizeof_base, 1, num1);
			//		}
			//		else
			//		{
			//			d1=0;
			//			fseek(num1, (len1-i)*sizeof_base, 0);
			//			fwrite(&d1, sizeof_base, 1, num1);
			//		}
			//}
			//else
			//{
			//	for (i=0; i<len1; i++)
			//		fseek(num2, i*sizeof_base, 0);
			//		fread(&d1, sizeof_base, 1, num2);
			//		fseek(num1, i*sizeof_base, 0);
			//		fwrite(&d1, sizeof_base, 1, num1);
			//}
			//fclose(num1);
			//fclose(num2);

			int l=strlen(a.bnum);
			bnum=new char[l];
			strcpy(bnum, a.bnum);
			//for (int i=0; i<l; i++)
			//	bnum[i]=a.bnum[i];
		}
		else 
		{
			dnum.minus=a.dnum.minus;
			dnum.amount=a.dnum.amount;
			dnum.digit=(int*)malloc(dnum.amount*sizeof(int));
			for (int i=0; i<dnum.amount; i++)
				dnum.digit[i]=a.dnum.digit[i];
		}
	}
	//������������
LongNum& LongNum::operator=(LongNum& a)
	{
		if (this==&a)
			return *this;
		bin=a.bin;
		if (bin)
		{
			rename_b(a.bnum);
			//strcpy(bnum, a.bnum);
			//FILE *num1, *num2;
			//if (((num1=fopen(bnum, "wb"))==NULL)||((num2=fopen(a.bnum, "rb"))==NULL))
			//	perror("Can not open File");
			//int len1, len2, i;
			//unsigned char d1;
			//fseek(num1, 0, SEEK_END);
			//len1=ftell(num1);
			//fseek(num2, 0, SEEK_END);
			//len2=ftell(num2);
			//if (len1>len2)
			//{
			//	for (i=0; i<len1; i++)
			//		if (i<len2)
			//		{
			//			fseek(num2, (len2-i)*sizeof_base, 0);
			//			fread(&d1, sizeof_base, 1, num2);
			//			fseek(num1, (len1-i)*sizeof_base, 0);
			//			fwrite(&d1, sizeof_base, 1, num1);
			//		}
			//		else
			//		{
			//			d1=0;
			//			fseek(num1, (len1-i)*sizeof_base, 0);
			//			fwrite(&d1, sizeof_base, 1, num1);
			//		}
			//}
			//else
			//{
			//	for (i=0; i<len1; i++)
			//		fseek(num2, i*sizeof_base, 0);
			//		fread(&d1, sizeof_base, 1, num2);
			//		fseek(num1, i*sizeof_base, 0);
			//		fwrite(&d1, sizeof_base, 1, num1);
			//}
			//fclose(num1);
			//fclose(num2);
		}
		else 
		{
			dnum.minus=a.dnum.minus;
			dnum.amount=a.dnum.amount;
			dnum.digit=(int*)malloc(dnum.amount*sizeof(int));
			for (int i=0; i<dnum.amount; i++)
				dnum.digit[i]=a.dnum.digit[i];
		}
		return *this;
	}
	//����������
LongNum::~LongNum()
	{
		if (!bin&&dnum.amount>0)
			free(dnum.digit);
		//else delete [] bnum; 
	}
	//������������� �������� ����
void LongNum::rename_b(char *newname)
{
	if (bin)
		rename(bnum, newname);
}
	//���� �� �����
void LongNum::input(char str[], bool is_bin)
	{
		bin=is_bin;
		if (bin)
		{
			int l=strlen(str);
			bnum=new char[l];
			strcpy(bnum, str);
			//for (int i=0; i<l; i++)
			//	bnum[i]=str[i];
			//bnum[l]='\0';
			//FILE *f=fopen(bnum, "wb");
			//fclose(f);
		}
		else 
		{
			FILE *f=fopen(str, "rt");
			if (f==NULL)
				perror("Can not open file");
			dnum=BI_input(f);
			fclose(f);
		}
	}

	//����� � ���������� ����
void LongNum::output(char *str)
	{
		if (!bin)
		{
			FILE *f;
			if ((f=fopen(str, "wt"))==NULL)
				perror("Can not open file");
			BI_output(dnum, f);
			fclose(f);
		}
		else printf("ERROR: output bin file");
	}


	//��������
LongNum LongNum::operator+(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_add(dnum, num2.dnum);
	else if (bin&&num2.bin)
	{
		tmp.bnum="tmpfile";
		BI_add_b(bnum, num2.bnum, tmp.bnum);
	}
	return tmp;
}
	//���������
LongNum LongNum::operator-(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_sub(dnum, num2.dnum);
	else if (bin&&num2.bin)
		BI_sub_b(bnum, num2.bnum, tmp.bnum);
	return tmp;
}
	//���������
LongNum LongNum::operator*(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_mul(dnum, num2.dnum);
	else if (bin&&num2.bin)
		BI_mul_b(bnum, num2.bnum, tmp.bnum);
	return tmp;
}
	//�������
LongNum LongNum::operator/(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_div(dnum, num2.dnum);
	else if (bin&&num2.bin)
		BI_div_b(bnum, num2.bnum, tmp.bnum);
	return tmp;
}
	//������� �� �������
LongNum LongNum::operator%(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_mod(dnum, num2.dnum);
	else if (bin&&num2.bin)
		BI_mod_b(bnum, num2.bnum, tmp.bnum);
	return tmp;
}
LongNum LongNum::deg_by_mod(LongNum deg, LongNum mod)
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!deg.bin&&!mod.bin)
		tmp.dnum=BI_deg(dnum, deg.dnum, mod.dnum);
	else if (bin&&deg.bin&&mod.bin)
		BI_deg_b(bnum, deg.bnum, mod.bnum, tmp.bnum);
	return tmp;
}

				//MAIN
int main(int argc, char *argv[])
{
	if (argc<5&&(argc>7))
	{
		perror("Wrong arguments!");
		return -1;
	}

	char operation;
	if (strlen(argv[2])>1)
	{
		perror("Wrong operator!");
		return -2;
	}
	else operation=argv[2][0];

	LongNum num1, num2, num0, mod;
	bool is_bin, by_mod;

	//���� ����� ��������
	if (((argc==6)&&(argv[5][0]=='-')&&(argv[5][1]=='b'))||((argc==7)&&(argv[6][0]=='-')&&(argv[6][1]=='b')))
	{
		is_bin=true;
		if (argc==7) by_mod=true;
		else by_mod=false;
	}
	else
	{
		is_bin=false;		
		if (argc==6) by_mod=true;
		else by_mod=false;
	}

	num1.input(argv[1], is_bin);
	num2.input(argv[3], is_bin);
	//if (is_bin) num0.input(argv[4], is_bin);

	if (by_mod)
		mod.input(argv[5], is_bin);

	if (operation=='+')
		num0=(num1+num2);	
	else if (operation=='-')
		num0=num1-num2;
	else if (operation=='*')
		num0=num1*num2;
	else if (operation=='/')
		num0=num1/num2;
	else if (operation=='%')
		num0=num1%num2;
	else if (operation=='^'&&by_mod)
		num0=num1.deg_by_mod(num2, mod);
	else 
	{
		perror("Wrong operator!\n");
		return 2;
	}

	if (is_bin)
	{
		num0.rename_b(argv[4]);
		//remove("tmpfile");
	}
	

	if (by_mod&&operation!='^')
	{
		LongNum tmp;
		tmp=num0%mod;
		tmp.output(argv[4]);
		//num0.rename_b(argv[4]);
		//remove("tmpfile");
	}
	else
	if (!is_bin) num0.output(argv[4]);

	return 0;
}